<?php
declare(strict_types=1);

class FizzBuzz
{
    private $from;
    private $to;

    public function __construct(Int $from, Int $to)
    {
        if(func_num_args() > 2)
        {
            throw new ArgumentCountError('Too many arguments passed');
        }

        if($from >= $to)
        {
            throw new InvalidArgumentException('FizzBuzz Range Incorrect');
        }

        if($from <1 || $to>100)
        {
            throw new InvalidArgumentException("FizzBuzz Range is 1 to 100; range given $from - $to");
        }

        // Checked inputs, save them
        $this->from=$from;
        $this->to=$to;
    }

    /**
     * Return the result of fizzbuzzing on the given range as a string
     *
     * @return string
     */
    public function report() :string
    {
        $result='';
        for($n = $this->from ; $n <= $this->to; $n++)
        {
            $result .= "$n\t";
            $result .= $this->tests($n);
            $result .= "\n";
        }
        return $result;
    }

    /**
     * Print the result of fizzbuzzing on the given range
     *
     * @return void
     */
    public function printReport() :void
    {
        print $this->report();
    }

    /**
     *
     *  Method to do the testing of each number. Allows subclassing
     *  to other types of tests.
     *
     * @return string
     */
    private function tests($n) :string
    {
        $result='';

        if($n % 3 === 0)
        {
            $result .= "fizz";
        }

        if($n % 5 === 0)
        {
            $result .= 'buzz';
        }

        return $result;
    }
}
