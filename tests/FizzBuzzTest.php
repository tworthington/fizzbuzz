<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;

class FizzBuzzTest extends TestCase
{
    public function testBasicFizzBuzzWithRange()
    {
        $this->assertInstanceOf(
            FizzBuzz::class,
            new FizzBuzz(1,100)
        );
    }

    public function testBasicFizzBuzzWithBadRange()
    {
        $this->expectException(InvalidArgumentException::class);
        new FizzBuzz(100,1);
    }

    public function testBasicFizzBuzzWithHighRange()
    {
        $this->expectException(InvalidArgumentException::class);
        new FizzBuzz(1,101);
    }

    public function testBasicFizzBuzzWithLowRange()
    {
        $this->expectException(InvalidArgumentException::class);
        new FizzBuzz(0,100);
    }

    public function testFailWithTooFewParams()
    {
        $this->expectException(ArgumentCountError::class);
        new FizzBuzz(100);
    }

    public function testFailWithTooManyParams()
    {
        $this->expectException(ArgumentCountError::class);
        new FizzBuzz(1,2,3);
    }

    public function testFailWithWrong1stArguementType()
    {
        $this->expectException(TypeError::class);
        new FizzBuzz('a',3);
    }

    public function testFailWithWrong2ndArguementType()
    {
        $this->expectException(TypeError::class);
        new FizzBuzz(1,'b');
    }

    public function testFailWithIntLikeString1stArguement()
    {
        $this->expectException(TypeError::class);
        new FizzBuzz('1',3);
    }

    public function testFailWithIntLikeString2ndArguement()
    {
        $this->expectException(TypeError::class);
        new FizzBuzz(1,'3');
    }

    public function testFailWithFloat1stArguement()
    {
        $this->expectException(TypeError::class);
        new FizzBuzz(1.0,3);
    }

    public function testFailWithFloat2ndArguement()
    {
        $this->expectException(TypeError::class);
        new FizzBuzz(1,3.0);
    }

    public function testBasicResultTypeIsString()
    {
        $buzzer=new FizzBuzz(1,100);

        $this->assertInternalType(
            'string',
            $buzzer->report()
        );
    }

    public function testSpecificResult()
    {
        $buzzer=new FizzBuzz(1,15);

        $expected= <<<EOT
1\t
2\t
3\tfizz
4\t
5\tbuzz
6\tfizz
7\t
8\t
9\tfizz
10\tbuzz
11\t
12\tfizz
13\t
14\t
15\tfizzbuzz\n
EOT;
        $this->assertEquals(
            $expected,
            $buzzer->report()
        );
    }

    public function testSpecificResultFail()
    {
        $buzzer=new FizzBuzz(1,16);

        $expected= <<<EOT
1\t
2\t
3\tfizz
4\t
5\tbuzz
6\tfizz
7\t
8\t
9\tfizz
10\tbuzz
11\t
12\tfizz
13\t
14\t
15\tfizzbuzz\n
EOT;
        $this->assertNotEquals(
            $expected,
            $buzzer->report()
        );
    }
}
